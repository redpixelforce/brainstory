<?php

namespace app\models;
use Yii;

class Statistic {


  	public function getNOKORating($number){
		if($number){
			$extraSQL = "LIMIT 0, $number";
		}
        $sql = "SELECT
					'Рейтинг НОКО' as SubjName,
					catalogSchools.ShortName,
					catalogSchools.UrlName,
					avg(answers.Answer) as Rate
				FROM answers
					INNER JOIN authorization
						ON answers.UserId = authorization.id
					INNER JOIN catalogSchools
						ON catalogSchools.Id = authorization.idSchool
				WHERE answers.QuestName NOT LIKE '9%'
				GROUP BY authorization.IdSchool
				ORDER BY Rate DESC 
				$extraSQL";
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);

$result = $command->queryAll();
		
		return $result;
	}
 	public function getSchoolRatingByUrlName($urlName, $year = 0){
		if($year){
			$extra = "AND rating.Year = $year";
		}else{
			$extra = "";
		}
		$sql = "SELECT
					rating.Id,
					rating.IdSubject,
					rating.IdSchool,
					rating.Year,
					rating.Rate,
					catalogSubjects.Id as SubjId,
					catalogSubjects.Name as SubjName
				FROM rating
					INNER JOIN catalogSubjects
						ON rating.IdSubject = catalogSubjects.Id
					INNER JOIN catalogSchools
						ON rating.IdSchool = catalogSchools.Id
				WHERE catalogSchools.UrlName = '$urlName' $extra
				ORDER BY catalogSubjects.Order";
         $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);

$result = $command->queryAll();
		return $result;
	}
   public function getSubjectRating($subj, $year, $number){
		if($number){
			$extraSQL = "LIMIT 0, $number";
		}
		$sql = "SELECT
					rating.Id,
					rating.IdSubject,
					rating.IdSchool,
					rating.Year,
					rating.Rate,
					catalogSchools.ShortName,
					catalogSchools.UrlName,
					catalogSubjects.Name as SubjName
				FROM rating
					INNER JOIN catalogSubjects
						ON rating.IdSubject = catalogSubjects.Id
					INNER JOIN catalogSchools
						ON rating.IdSchool = catalogSchools.Id
				WHERE catalogSubjects.UrlName = '$subj' AND rating.Year = $year
				ORDER BY rating.Rate DESC 
				$extraSQL";
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);

        $result = $command->queryAll();
		return $result;
   }
}
