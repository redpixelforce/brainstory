<?php
namespace app\models;

use yii\base\Widget;
use yii\helpers\Html;
use app\models\User;
use app\models\Chat;
use yii\web\Request;
use Yii;
class StatWidget extends Widget {
    public $sourcePath = '@app/web/';
    public $css = [
    ];
    public $js = [ // Configured conditionally (source/minified) during init()
        'js/stat.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    private $subjects;
    public function init()
    {
        parent::init();
        $this->subjects = Catalogsubjects::find()->where(['Deleted' => 0])->orderBy('Order ASC')->all();
    }

    public function run()
    {
    return $this->render('stat_view', [
            'subjects' =>  $this->subjects,
            'url' => \yii\helpers\Url::to(['/statistic/load-statistic']),
        ]);
    }
    

    
    public static function sendChat($project_id, $message) {
        $model = new Chat();

        if ($message!=null) {
        $model->userId = Yii::$app->user->identity->id;
        $model->project_id = $project_id;
        $model->message = $message;
        if ($model->save()) {
                
                echo $model->getData();
            } else {
                print_r($model->getErrors());
                exit(0);
            }
        }
        echo $model->getData();
    }
}