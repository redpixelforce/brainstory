<?php 
namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Html;
 use app\models\Catalogschools;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegistrationForm extends Model
{
    public $username;
    public $password;
    public $password_repeat;
    public $status;
    public $school;
    public function rules() {
        return [
          [['username','status','school'],'required', 'message' => 'Это поле обязательно к заполнению!'],  
        ['password', 'compare','message' => 'Пароли должны совпадать!'],

          ['username','uniqueName','message'=>'Такой username уже есть в базе данных!'],
              ['password', 'required','message' => 'Это поле обязательно к заполнению!'],
        ['password', 'string', 'min' => 6,'message' => 'Это поле обязательно к заполнению!'],
        ['password_repeat', 'required','message' => 'Это поле обязательно к заполнению!'],
                
                        
        ];
    }
function createHash($str){
    $salt = str_replace('=', '', base64_encode(md5('26843159712521FD37EAA5ED9565683326EA85DCD0E59')));
    $iteration = 100;
    for($i = 0; $i < $iteration; $i++)
      $str = sha1($str . $salt);
    return $str;
  }
      public function compare_password($attribute, $params) {
       
        if (!$password==$password_repeat) {
             $this->addError($attribute,'Такой username уже есть в базе данных!');
        }
    }
    public function uniqueName($attribute, $params) {
        $names = User::find()
    ->where(['login' => Html::encode($this->username)])
    ->count();
        if ($names>0) {
             $this->addError($attribute,'Такой username уже есть в базе данных!');
        }
    }
    public function validateRepit() {
        if (!empty($repitPassword) && ($repitPassword===$password)) {
            
        }
        
    }

    public function registrationUser() {
            $status1 = $this->status;
            if ($status1 == 0) {
                $status1="student";
    
            }
            if ($status1 == 1) {
                $status1="parent";
    
            }
            if ($status1 == 2) {
                $status1="teacher";
    
            }
        $schoolName = Catalogschools::findOne(['Id' => $this->school])->ShortName;
            $login = Html::encode($this->username);
            $password1 = Html::encode($this->password);
            $school1 = Catalogschools::findOne(['ShortName' => $schoolName])->Id;
            
            $user = new User();
            
            $user->login=$login;
            $hash = $this->createHash($password1);
            $user->password=$hash;
            $user->deleted = 0;
            $user->status = $status1;
            $user->idSchool = $school1;
            $user->save();
            return true;
        
    

}
   

}
?>

