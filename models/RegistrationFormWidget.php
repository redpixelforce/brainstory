<?php
 
namespace app\models;
 
use Yii;
use yii\base\Widget;
use app\models\RegistrationForm;
 
class RegistrationFormWidget extends Widget {
 
    public function run() {
        if (Yii::$app->user->isGuest) {
            $model = new RegistrationForm();
            return $this->render('registrationFormWidget', [
                'model' => $model,
            ]);
        } else {
            return ;
        }
    }
}