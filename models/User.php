<?php
namespace app\models;
use Yii;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;
    public static function tableName()
    {
        return 'authorization';
    }
function createHash($str){
    $salt = str_replace('=', '', base64_encode(md5('26843159712521FD37EAA5ED9565683326EA85DCD0E59')));
    $iteration = 100;
    for($i = 0; $i < $iteration; $i++)
      $str = sha1($str . $salt);
    return $str;
  }
    
    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
   
public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
 
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
 
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username, 'deleted' => self::STATUS_ACTIVE]);
    }
 
    /**
     * @inheritdoc
     */
   
    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
   
    /**
     * @return int|string current user ID
     */
      public function validatePassword($password)
    {
       return ($this->createHash($password)=== $this->password);
    }
 
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
//        return $this->auth_key;
    }
  
    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
//        return $this->getAuthKey() === $authKey;
    }
}