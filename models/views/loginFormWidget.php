<?php
 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
 
Modal::begin([
    'header'=>'<h4>Логин</h4>',
    'id'=>'login-modal',
]);
?>
 
    <p>Пожалуйста, заполните следующие поля для авторизации:</p>
 
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'enableAjaxValidation' => true,
    'action' => ['site/ajax-login'],
        'validateOnSubmit'     => true,

]);
echo $form->field($model, 'username')->textInput()->label("Логин");
echo $form->field($model, 'password')->passwordInput()->label("Пароль");
?>
 
<div>
   Не зарегистрированы? <?= Html::a('Зарегистрируйтесь', ['#'], ['data-toggle' => 'modal', 'data-target' => '#registration-modal','data-dismiss' => 'modal']) ?>.
</div>
<div class="form-group">
    <div class="text-right">
 
        <?php
        echo Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal', 'style' => 'margin: 10px;']);
        echo Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button','style' => 'margin: 10px;']); 
        ?>
 
    </div>
</div>
 
<?php 
ActiveForm::end();
Modal::end();