
<div class="container">
  <div class="row">
    <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d73773.49015695116!2d20.463665466687605!3d54.702205921550906!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46e33d8d4b7c21a9%3A0x5050960016126ed3!2z0JrQsNC70ZbQvdGW0L3SkdGA0LDQtCwg0JrQsNC70ZbQvdGW0L3Qs9GA0LDQtNGB0YzQutCwINC-0LHQu9Cw0YHRgtGMLCDQoNC-0YHRltGP!5e0!3m2!1suk!2sua!4v1485925218671" width="100%" height="450" frameborder="0" style="border:0; margin-top:-20px;" allowfullscreen></iframe> -->

    <!--Chart start here-->
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <h1 class="chart-header">Рейтинг школ по результатам ЕГЭ</h1>
        <hr class="chart-hr">
        <div class="clear"></div>
        <div id="year">
          <div class="btn btn-default year" data-year="2015" data-url = "<?=$url?>">2015</div>
          <div class="btn btn-edu year" data-year="2016" data-url = "<?=$url?>">2016</div>
          <div class="btn btn-default year" data-year="2017" data-url = "<?=$url?>">2017</div>
        </div>
        <div class="col-lg-12 way">
          <div id="subj">
            <ul class="col-lg-3 type-of" style="text-align: right;">
              <li style="color: #b4b4b4;">Выберите предмет:</li>
              <?php
                foreach($subjects as $subject){
                  ?>
                  <li data-subj="<?= $subject->UrlName?>" data-url = "<?=$url?>"><?= $subject->Name?></li>
                  <?php
                }
              ?>
              <li data-subj="noko" id="noko" data-url = "<?=$url?>">Рейтинг НОКО</li>
            </ul>
          </div>
          <div id="ratingTable" class="col-lg-7 rating">
              
          </div>
        </div>
      </div>
    <!--End of chart field-->
    <div class="clear h40"></div>
  </div>
</div>

<script type="text/javascript">
  document.title = "Образование в Калининграде | Рейтинг школ Калининграда";
</script>


<?php
$this->registerJsFile('@web/js/stat.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>