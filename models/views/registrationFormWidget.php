<?php
 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
 use app\models\Catalogschools;
use yii\helpers\ArrayHelper;
Modal::begin([
    'header'=>'<h4>Регистрация</h4>',
    'id'=>'registration-modal',
]);
?>
 
    <p>Пожалуйста, заполните следующие поля для авторизации:</p>
 
<?php $form = ActiveForm::begin([
    'id' => 'registration-form',
    'enableAjaxValidation' => false,
//        'validateOnChange' => true,
//        'validateOnSubmit'     => true,
//    'enableClientValidation' => true,
    
    'action' => ['site/registration'],
//    'validationUrl' => ['site/validate-registration'],

]);
  $catalogschools = Catalogschools::find()->all();
    $items = ArrayHelper::map($catalogschools,'Id','ShortName');

echo $form->field($model, 'username')->textInput()->label("Логин");
echo $form->field($model, 'password')->passwordInput()->label("Пароль");
echo $form->field($model, 'password_repeat')->passwordInput()->label("Повторите пароль");
echo $form->field($model, 'status')->dropDownList([
    '0' => 'Ученик',
    '1' => 'Учитель',
    '2'=>'Родитель'
]);

echo $form->field($model, 'school')->dropDownList($items);

?>
 

<div class="form-group">
    <div class="text-right">
 
        <?php
        echo Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal', 'style' => 'margin: 10px;']);
        echo Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'registration-button','style' => 'margin: 10px;']); 
        ?>
 
    </div>
</div>
 
<?php 
ActiveForm::end();
Modal::end();