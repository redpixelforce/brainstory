<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
      'css/site.css',
        'css/style.css',
        'css/lightbox.css',
        'css/designer.css',
        'css/font-awesome.css',
        'css/bootstrap-datetimepicker.min.css',
        
    ];
    public $js = [
        '/js/jquery.mobile.custom.js',
    '/js/moment-with-locales.min.js',
    '/js/bootstrap-datetimepicker.min.js',
    '/js/lightbox.js',
    '/js/flot/jquery.flot.js',
    '/js/flot/jquery.flot.categories.js',
    '/js/charts/charts.js',
    '/js/salvattore.js',
        'js/file-input.js'

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
