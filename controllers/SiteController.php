<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Articles;
use app\models\Catalogcenters;
use app\models\Authors;
use app\models\Catalogsubjects;
use app\models\RegistrationForm;
use app\models\ConstructorForm;
use yii\web\UploadedFile;
use App\Http\Requests;
use yii\web\Request;
class SiteController extends Controller
{
    public $stringImage;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
              'only' => ['ajax-login','addarticle','registration'],
                'rules' => [
                    [
                        'actions' => ['addarticle'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
             [
                        'allow' => true,
                        'actions' => ['ajax-login', 'registration'],
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action) {
    if ($action == 'redirect') {
        $this->enableCsrfValidation = false;
    }
    return parent::beforeAction($action);
}
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
         $session = Yii::$app->session;
// первый вариант
                        $session->set('imgString',  "/images/noimage.png");
        $articles = Articles::find()->where(['Id_catalogArticlesType' => 1])->limit(3)->orderBy('DateOfCreation DESC, Id DESC')->all();
        $centers = CatalogCenters::find()->all();
        return $this->render('index', ['articles' => $articles, 'centers' => $centers]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
  public function actionAjaxLogin() {
    if (Yii::$app->request->isAjax) {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                return $this->goBack();
            } else {
                Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
                return \yii\widgets\ActiveForm::validate($model);
            }
        }
    } else {
        throw new \yii\web\HttpException(404 ,'Page not found');
    }
}
      public function actionValidateRegistration() {
          $model = new RegistrationForm();
                  if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

}
      public function actionRegistration() {
        $model = new RegistrationForm();
    $request = Yii::$app->getRequest();
    if ($request->isPost && $model->load($request->post()) && $model->validate()) {
       
        //сохранение данных в бд 
           $model->registrationUser();
           return $this->render('about');

        }
            
        return $this->goBack();

      

      }
    public function actionAddarticle() {
        $type = Yii::$app->request->get('type','articles');
        $model = new ConstructorForm();
        $strImage = "";
        if ($model->load(Yii::$app->request->post())) {
         
             if ($model->validate()) {

                if ($type == 'news') {
                    $type1 = 2;
                } else {
                    $type1 = 1;
                }
                                          $session = Yii::$app->session;

                 $model->saveArticle($type1,$session->get('imgString'));
                $session->set('imgString',  "/images/noimage.png");

                return $this->render('about');
             }
         }
        return $this->render('constructor',['model'=> $model, 'type' => $type]);
    }
    public function actionFileUpload() {
         $model = new ConstructorForm();

        if (Yii::$app->request->isAjax) {
            $type = Yii::$app->request->post('type');
            $model->image = UploadedFile::getInstances($model, 'image');
            if ($model->image!=null) {
                     foreach ($model->image as $file) {

                $name_image = uniqid().'.';
                $path = Yii::$app->params['pathUploads'] . 'images/'.$type.'/'.$name_image;
                $file->saveAs($path.$file->extension);
                 $name_image = $name_image.$file->extension;
                         $session = Yii::$app->session;
// первый вариант
                        $session->set('imgString',  "/".$path.$file->extension);
//                 $project->image = $name_image;   
//                         return $this->render('constructor',['model'=> $model,'url' => 'sfdsfsd']);
                return '{"response": "'.$path.$file->extension.'"}';
            }
            }
        }
    }
    
  
    public function actionSchools() {
        $subjects = Catalogsubjects::find()->where(['Deleted' => 0])->orderBy('Order ASC')->all();
        return $this->render('schools',['subjects' => $subjects]);
    }
    public function actionArticles() {
        $request = Yii::$app->request;
        $tag = $request->get('tag',''); 
        $title = "Все статьи";
        $author1 = $request->get('author','no author'); 
        
        if ($tag!='') {
            $title.="по тегу #".$tag; 
        }
        if ($author1==='no author') {
            $articles = Articles::find()->where(['Id_catalogArticlesType' => 1])->andWhere(['like', 'Tags', $tag])->orderBy('DateOfCreation DESC, Id DESC')->all();
        } else {
            $author = Authors::findOne(['AuthorUrl' => $author1]);
            $author_name = $author->AuthorNameBy;
            $title.=" автора ".$author_name;
            $articles = Articles::find()->where(['Id_catalogArticlesType' => 1, 'AuthorId' => $author->AuthorId])->andWhere(['like', 'Tags', $tag])->orderBy('DateOfCreation DESC, Id DESC')->all();

        }

        return $this->render('articles', [
            'articles' => $articles,
            'title' => $title
        ]); 
        
    }
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    public function actionArticle()
    {
         $request = Yii::$app->request;
        $furl = $request->get('name',''); 
        $article = Articles::findOne(['FURL' => $furl]);
        if ($article == null) {
            return $this->render('error');
        }    
        $author = Authors::findOne(['AuthorId' => $article->AuthorId]);
        
        return $this->render('article', ['article' => $article, 'author' => $author]);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
//    public function actionContact()
//    {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//            Yii::$app->session->setFlash('contactFormSubmitted');
//
//            return $this->refresh();
//        }
//        return $this->render('contact', [
//            'model' => $model,
//        ]);
//    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
