<?php

namespace app\controllers;
use yii\helpers\Html;
use app\models\Statistic;
use yii\web\Request;

use Yii;
use yii\web\Controller;

class StatisticController extends Controller
{
public function actionLoadStatistic() {
    $this->enableCsrfValidation = false;
    $request = Yii::$app->request;

    $subject = $request->post('subject', false);   
        
        $statistic = new Statistic;
        if($subject){
        if($subject == 'noko'){
            $ratings = $statistic->getNOKORating($request->post('number',false));
        }else{
    	   $ratings = $statistic->getSubjectRating($subject, $request->post('year',false),$request->post('number'));    
        }
    }else{
    	$ratings = $statistic->getSchoolRatingByUrlName($request->post('urlName',false),$request->post('year','false'));
    }

	$req = [];
	foreach($ratings as $rating){
		$obj = (object) $rating;
		array_push($req, $obj); 
	}
	echo json_encode($req);
        
    }

 public function beforeAction($action)
    {
        // ...set `$this->enableCsrfValidation` here based on some conditions...
        // call parent method that will check CSRF if such property is true.
        if ($action->id === 'load-statistic') {
            # code...
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    } 
}