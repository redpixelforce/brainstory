<?php

use yii\helpers\Url;
use app\models\DateManager;
?>


<div class="container">
  <div class="row">
    <!--News part start here-->
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <h3 class="article-id"><strong><?=$title?></strong></h3>
         <hr class="article-header">
          <?php
            foreach($articles as $article){
            $date = DateManager::toLongDate($article->DateOfCreation);
              ?>
              <div class="way col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <a href=""><img src="<?=$article->ImgPath ?>" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 img-responsive"></a>
                <div class="article articles col-lg-8 col-md-8 col-sm-8 col-xs-12">
                   <a href="<?=Url::to(['site/article', 'name' => $article->FURL]) ?>"><h3 class="article-semi-head"><?=$article->Title; ?></h3></a>
                   <?php
                      if($article->Tags){
                        $tags = explode(" ", $article->Tags);
                        $tags = explode(" ", $article->Tags);
                        $tagsStr = "<div class='tags pull-left'><ul><li><b>Теги &emsp;</b></li>";
                        foreach($tags as $tag){
                          $tagsStr .= "<li class='tagItem'><a href='".Url::to(['site/articles', 'tag' => $tag])."'>&emsp;$tag&emsp;</a></li>";
                        }
                        $tagsStr .= "</ul></div>";
                        echo $tagsStr;
                      }
                    ?>
                    <div class="clear h10"></div>
                   <?php echo htmlspecialchars_decode($article->PreviewText); ?>
                   <div class="clear h10"></div>
                   <p><i><?php echo $date; ?></i></p>
                    <?php
                      if($_SESSION['user']['Status'] == 'admin'){
                        ?>
                        <a href="/addarticle?mode=edit&type=articles&id=<?php echo $article->Id;?>"><i class="fa fa-edit"></i> Редактировать</a> | <a href="/deletearticlefromdb?id=<?php echo $article->Id;?>"><i class="fa fa-remove"></i> Удалить</a>
                        <?php
                      }
                    ?>
                   <a href="<?=Url::to(['site/article', 'name' => $article->FURL]) ?>" class="pull-right">Читать дальше <i class="fa fa-angle-double-right"></i></a>
                </div>
              </div>
              <?php
            }
          ?>
      </div>
      

           <!-- <div class="text-center">
              <ul class="pagination pagination-edu">
              <li>
                <a href="#" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
              <li><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li>
                <a href="#" aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
            </ul>
                     </div> -->

    <!--End of news part-->
  </div>
</div>

