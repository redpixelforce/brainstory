<?php

use yii\helpers\Url;
use app\models\DateManager;
?>

<!-- модальное окно  -->


<!-- модальное окно  -->



<div class="mobile-slider">
  <div id="carousel" class="carousel slide" data-ride="carousel" >

  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="/images/slider/first-bg.jpg" class="img-responsive" alt="...">
      <div class="carousel-caption">
        <div class="col-lg-12">
          <h1>BrainStory</h1>
          <h3>Всё, что вы хотели знать об истории, но боялись ЕГЭ</h3>
         
        </div>
      </div>
    </div>
    
</div>

</div>
</div>


<div class="container">

    <!--Article part start here-->
   <div class="mainpage-part col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <h3 class="article-id"><strong>Статьи</strong></h3>
     <hr class="article-header">
        <?php
        
          foreach($articles as $article) {
            $date = DateManager::toLongDate($article->DateOfCreation);
            ?>
            <!-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 way"> -->
            <div class="way col-lg-4 col-md-4 col-sm-6 col-xs-12">
             <a href="<?=Url::to(['site/article', 'name' => $article->FURL]) ?>"><img src="<?=$article->ImgPath ?>" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 img-responsive"></a>
              <div class="article articles text-part col-lg-12">
                 <a href="<?=Url::to(['site/article', 'name' => $article->FURL]) ?>"><h3 class="article-semi-head"><?=$article->Title ?></h3></a>
                  <?php
                    if($article->Tags){
                      $tags = explode(" ", $article->Tags);
                      $tagsStr = "<div class='tags pull-left'><ul><li><b>Теги &emsp;</b></li>";
                      foreach($tags as $tag){
                        $tagsStr .= "<li class='tagItem'><a href='".Url::to(['site/articles', 'tag' => $tag])."'>&emsp;$tag&emsp;</a></li>";
                      }
                      $tagsStr .= "</ul></div>";
                      echo $tagsStr;
                    }
                  ?>
                  <div class="clear h10"></div>
                 <?php echo htmlspecialchars_decode($article->PreviewText); ?>
                 <div class="clear h10"></div>
                 <p><i><?= $date; ?></i></p>
               
                 <a href="<?=Url::to(['site/article', 'name' => $article->FURL]) ?>" class="pull-right">Читать дальше <i class="fa fa-angle-double-right"></i></a>
                </div>
                <div class="clear"></div>
            </div>
            <?php
          }
        ?>
      
    
      <div class="clear h10"></div>
      <div class="text-center">
           <a href="/articles" class="btn btn-way">ВСЕ СТАТЬИ</a>
      </div>
    </div>
    <div class="mainpage-part col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <h3 class="article-id"><strong>Игры</strong></h3>
     <hr class="article-header">
      <div class="masonry" data-columns>
        <div class="way">
         <a href="/nback"><img src="<?=Url::home(true); ?>/images/game1.png" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 img-responsive"></a>
          <div class="article articles text-part col-lg-12">
             <a class="text-center" href="/nback"><h3 class="article-semi-head">N-back</h3></a>
          </div>
          <div class="clear"></div>
        </div>
        <div class="way">
         <a href="/memory"><img src="<?=Url::home(true); ?>/images/game1.png" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 img-responsive"></a>
          <div class="article articles text-part col-lg-12">
             <a class="text-center" href="/memory"><h3 class="article-semi-head">Memory</h3></a>
          </div>
          <div class="clear"></div>
        </div>
      </div>
   </div>

   <div class="mainpage-part col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <h3 class="article-id"><strong>Товары</strong></h3>
     <hr class="article-header">
        <div class="way col-lg-4 col-md-4 col-sm-6 col-xs-12">
         <a href="<?=Url::to(['site/article', 'name' => $article->FURL]) ?>"><img src="<?=Url::home(true); ?>/images/game2.png" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 img-responsive"></a>
          <div class="article articles text-part col-lg-12">
             <a href=""><h3 class="article-semi-head">Товар 1</h3></a>
             Описание товара
             <div class="clear h10"></div>
             <a href="" class="btn btn-way" style="font-size: 13px">Заказать</a>
             <a href="" class="btn btn-way pull-right" style="font-size: 13px">Подробнее</a>
            </div>
            <div class="clear"></div>
        </div>

        <div class="way col-lg-4 col-md-4 col-sm-6 col-xs-12">
         <a href="<?=Url::to(['site/article', 'name' => $article->FURL]) ?>"><img src="<?=Url::home(true); ?>/images/game2.png" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 img-responsive"></a>
          <div class="article articles text-part col-lg-12">
             <a href=""><h3 class="article-semi-head">Товар 1</h3></a>
             Описание товара которое занимает более одной строки потому, что оно достаточно обширное
             <div class="clear h10"></div>
             <a href="" class="btn btn-way" style="font-size: 13px">Заказать</a>
             <a href="" class="btn btn-way pull-right" style="font-size: 13px">Подробнее</a>
            </div>
            <div class="clear"></div>
        </div>

        <div class="way col-lg-4 col-md-4 col-sm-6 col-xs-12">
         <a href="<?=Url::to(['site/article', 'name' => $article->FURL]) ?>"><img src="<?=Url::home(true); ?>/images/game2.png" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 img-responsive"></a>
          <div class="article articles text-part col-lg-12">
             <a href=""><h3 class="article-semi-head">Товар 1</h3></a>
             Описание товара,
             <div class="clear h10"></div>
             <a href="" class="btn btn-way" style="font-size: 13px">Заказать</a>
             <a href="" class="btn btn-way pull-right" style="font-size: 13px">Подробнее</a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>

        <div class="way col-lg-4 col-md-4 col-sm-6 col-xs-12">
         <a href="<?=Url::to(['site/article', 'name' => $article->FURL]) ?>"><img src="<?=Url::home(true); ?>/images/game2.png" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 img-responsive"></a>
          <div class="article articles text-part col-lg-12">
             <a href=""><h3 class="article-semi-head">Товар 1</h3></a>
             Описание товара
             <div class="clear h10"></div>
             <a href="" class="btn btn-way" style="font-size: 13px">Заказать</a>
             <a href="" class="btn btn-way pull-right" style="font-size: 13px">Подробнее</a>
            </div>
            <div class="clear"></div>
        </div>

        <div class="way col-lg-4 col-md-4 col-sm-6 col-xs-12">
         <a href="<?=Url::to(['site/article', 'name' => $article->FURL]) ?>"><img src="<?=Url::home(true); ?>/images/game2.png" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 img-responsive"></a>
          <div class="article articles text-part col-lg-12">
             <a href=""><h3 class="article-semi-head">Товар 1</h3></a>
             Описание товара
             <div class="clear h10"></div>
             <a href="" class="btn btn-way" style="font-size: 13px">Заказать</a>
             <a href="" class="btn btn-way pull-right" style="font-size: 13px">Подробнее</a>
            </div>
            <div class="clear"></div>
        </div>
      
    
      <div class="clear h20"></div>
      <div class="text-center">
           <a href="/articles" class="btn btn-way">ВСЕ ТОВАРЫ</a>
      </div>
   </div>



<!--News part start here-->
  <!-- <div class="mainpage-part col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <h3 class="article-id"><strong>Новости</strong></h3>
     <hr class="article-header">
       
        <div class="text-center" >
         <a href="/news" class="btn btn-way">ВСЕ НОВОСТИ</a>
       </div>
  </div> -->

  <!--End of news field-->

  <!--Chart start here-->
  <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center mainpage-part">
    <h1 class="chart-header">Рейтинг школ по результатам ЕГЭ</h1>
    <hr class="chart-hr">
    <div class="clear"></div>
    <div id="year">
      <div class="btn btn-default year" data-year="2015">2015</div>
      <div class="btn btn-edu year" data-year="2016">2016</div>
      <div class="btn btn-default year" data-year="2017">2017</div>
    </div>
    <div class="col-lg-12 way">
      <div id="subj">
        <ul class="col-lg-3 type-of" style="text-align: right;">
          <li style="color: #b4b4b4;">Выберите предмет:</li>
      
          <li data-subj="noko" id="noko">Рейтинг НОКО</li>
        </ul>
      </div>
      <div id="ratingTable" class="col-lg-7 rating">
          
      </div>
    </div>
    <div class="text-center">
      <a href="/schools" class="btn btn-way">ПОЛНЫЙ РЕЙТИНГ</a>
    </div>
  </div> -->
<!--End of chart field-->

<!--Education center start here-->
  <!-- <div class="mainpage-part col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <h3 class="article-id"><strong>Образовательные центры</strong></h3>
     <hr class="article-header">
  
    <?php
      
        foreach($centers as $center){
          ?>
          <div class="way col-lg-4 col-md-4 col-sm-4 col-xs-12" >
            <a href="/center/<?=$center->UrlName?>"><img src="images/article-bg-1.png" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 img-responsive"></a>
            <div class="article col-lg-12">
               <a href="/center/<?$center->UrlName?>"><h3 class="article-semi-head"><?=$center->ShortName?></h3></a>
               <div class="clear h10"></div>  
               <p><i class="fa fa-map-marker " aria-hidden="true"></i>&nbsp;<?=$center->Address?></p>
               <p><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;<?=$center->Phone?></p>
            </div>
         </div>
          <?php
        }
      ?>
      <div class="clear"></div>
       <div class="text-center">
         <a href="/centers" class="btn btn-way">ВСЕ ЦЕНТРЫ</a>
       </div>
  </div> -->
  <div class="clear h40"></div>
<!--End of education center field -->   
  </div>
<!--
<script>
  $('#submitLogin').click(function(e){
          e.preventDefault();
          var logIn = loginAJAX($(this).parent().prev('.modal-body').find('#logLogin').val(), $(this).parent().prev('.modal-body').find('#logPassword').val());
          console.log('LogIn: ' + logIn);
          if(logIn == true){
            $('#modalLogin').modal('hide');
            location.reload();
          } 
        });


</script>
-->
<script type="text/javascript">
  document.title = "BrainStory | Главная";
</script>


