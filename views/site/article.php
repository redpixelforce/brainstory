<?php

use yii\helpers\Url;
use app\models\DateManager;
?>


<div class="container">
  <div class="row">
    <!--Full article start here-->
      <div class="article col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <img src="<?=$article->ImgPath?>" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 img-responsive">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class=""><?=$article->Title?></h1>
              <?php
                      if($article->Tags){
                        $tags = explode(" ", $article->Tags);
                        $tags = explode(" ", $article->Tags);
                        $tagsStr = "<div class='tags pull-left'><ul><li><b>Теги &emsp;</b></li>";
                        foreach($tags as $tag){
                          $tagsStr .= "<li class='tagItem'><a href='".Url::to(['site/articles', 'tag' => $tag])."'>&emsp;$tag&emsp;</a></li>";
                        }
                        $tagsStr .= "</ul></div>";
                        echo $tagsStr;
                      }
                    ?>
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
              <?php 
                $decodePreview = htmlspecialchars_decode($article->PreviewText);
                echo $decodePreview; 
              ?>
            </div>
            <div class="clear"></div>
            <hr class="article-header">     
          </div>
          <div class="article-body col-lg-10 col-md-10 col-sm-12 col-xs-12">
            

            <!-- Репост в соцсети -->
            
            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
            <script src="//yastatic.net/share2/share.js"></script>
            <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus" data-counter="" data-title="<?= $article->Title?>" data-description="<?= $article->PreviewText?>"></div>
            
            <!-- *********** -->

            <div class="clear h10"></div>
            <?= htmlspecialchars_decode($article->ArticleText); ?>
            <div class="clear h10"></div>
            <!-- Репост в соцсети -->
      
            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
            <script src="//yastatic.net/share2/share.js"></script>
            <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus" data-counter="" data-title="<?= $article->Title?>" data-description="<?= $article->PreviewText ?>"></div>
            
            <!-- *********** -->
            <div class="clear h10"></div>
            <p><i><?php echo $date; ?></i></p>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 author">
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <img src="<?=Url::home(true); ?>/images/authors/<?php echo $author->AuthorUrl?>.jpg" alt="" class="img-responsive">
              </div>
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                <h5><a href="<?=Url::to(['site/articles', 'author' => $author->AuthorUrl]) ?>"><?= $author->AuthorName?></a></h5>
                <p><?= $author->AuthorDescription?></p>
                <div class="clear h10"></div>
                <a href="<?=Url::to(['site/articles', 'author' => $author->AuthorUrl]) ?>">Все статьи автора</a><div class="pull-right"><!-- <a href="<?= $author->AuthorVKUrl?>"><i class="fa fa-vk"></i></a> --></div>
                <div class="clear h10"></div>
              </div>
            </div>
          </div>
          <div class="clear h40"></div>       
      </div>

    <!--End of full article-->

    <!-- DISQUS -->
      
      <div id="disqus_thread"></div>
      <script>
      
       /**
      *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
      *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
      
//      var disqus_config = function () {
//      this.page.url = window.location.href;  // Replace PAGE_URL with your page's canonical URL variable
//      this.page.identifier = 'article' + <?php echo $article['Id']?>; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
//      };
//      
//      (function() { // DON'T EDIT BELOW THIS LINE
//      var d = document, s = d.createElement('script');
//      s.src = '//edu39.disqus.com/embed.js';
//      s.setAttribute('data-timestamp', +new Date());
//      (d.head || d.body).appendChild(s);
//      })();
      </script>
      <!-- ********************** -->

  </div>
</div>

<script type="text/javascript">
  document.title = "<?php echo $article->Title?>";
</script>
