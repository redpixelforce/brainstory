<?php
  $NOKOresp = counter($db->getNOKORespondents(0));
  $schoolNOKO = $db->getSchoolNOKOAvg(0);
  $schoolNOKO = round($schoolNOKO['SchoolAvg'], 2);
?>

<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <h3 class="article-id"><strong>Информация о РЕЙТИНГЕ НОКО</strong></h3>
         <hr class="article-header">
          
          
          <div class="article">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
            <p>Proin pharetra nonummy pede. Mauris et orci. Aenean nec lorem. In porttitor. Donec laoreet nonummy augue. Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend.</p>
          </div>
          <div class="clear h20"></div>

          <a href="/testnoko" class="btn btn-way">Перейти к опросу <i class="fa fa-angle-double-right"></i></a>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <h1 class="chart-header">Рейтинг НОКО</h1>
            <hr class="chart-hr">
            <div class="col-lg-12">
              <div id="ratingTable" class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12 rating">
                <ul class="school-rating">
                  
                </ul> 
              </div>
              <div class="clear"></div>
              <a id="showRating" class="btn btn-way">Развернуть рейтинг</a>
            </div>
          </div>

          <div class="clear h40"></div>

          <div class="noko text-center" style="font-size: 14px;">
            <h1 class="chart-header">Средние показатели НОКО школ Калининграда</h1>
            <hr class="chart-hr">
            <?php

              $blockRatingsArray = [];
              for($i = 1; $i < 9; $i++){
                $blockRating = $db->getNOKOBlockRating($i, 0);
                if($blockRating){
                  array_push($blockRatingsArray, $blockRating['BlockAvg']);
                }
              }
              $jsonBlockRatingsArray = json_encode($blockRatingsArray);

              $questRatingsArray = [];
              for($i = 1; $i <= 8; $i++){
                $blockArray = [];
                for($j = 1; $j <= 11; $j++){
                  $questRating = $db->getNOKOQuestRating("$i.$j", 0);
                  array_push($blockArray, $questRating['QuestAvg']);
                }
                array_push($questRatingsArray, $blockArray);
              }
              $jsonQuestRatingsArray = json_encode($questRatingsArray);

              $block9RatingsArray = [];
              for($i = 1; $i <= 7; $i++){
                $questArray = [];
                $questRating = $db->getPercentQuest("9.$i", 0);
                foreach($questRating as $item){
                  $questArray[$item['Answer']] = $item['Qty'];
                }
                array_push($block9RatingsArray, $questArray);
              }
              $jsonBlock9RatingArray = json_encode($block9RatingsArray);

              if(count($blockRatingsArray) == 0){
                ?>
                <p>Для этой школы еще нет результатов</p>
                <?php
              }else{
                ?>
                <div id="ratingsNOKO">
                  <div id="NOKOBar" class="col-lg-6 col-lg-offset-3 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                    <div class="subject">
                      <h5 class="text-center">
                        <strong>Рейтинг НОКО</strong>
                      </h5>
                      <div class="progress" style="margin-top: 5px;">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $schoolNOKO;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $schoolNOKO * 10;?>%"><?php echo $schoolNOKO;?></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <canvas id="blocksChart" style="width: auto; height: auto;"></canvas>
                    <div id="js-blocklegend0" class="chart-legend"></div> 
                  </div>
                  <?php
                    for($i = 1; $i <= 8; $i++){
                      ?>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">                        
                        <canvas id="block<?php echo $i;?>Chart" style="width: auto; height: auto;"></canvas>
                        <div id="js-blocklegend<?php echo $i;?>" class="chart-legend"></div>
                      </div>
                      <?php
                      if($i % 2 != 0){
                        echo '<div class="clear"></div>';
                      }
                    }
                  ?>
                  <div class="clear"></div>
                  <h4 class="text-center">Сведения о респондентах НОКО<br>Число респондентов: <?php echo $NOKOresp;?></h4>
                  <?php
                    if(count($blockRatingsArray) > 0){
                      for($i = 0; $i < 7; $i++){
                        ?>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <div class="pieChartTitle"></div>                         
                          <canvas id="pieChart<?php echo ($i+1);?>" style="width: auto; height: auto;"></canvas>
                          <div id="js-pielegend<?php echo ($i+1);?>" class="chart-legend"></div>
                        </div>
                        <?php
                        if($i % 2 != 0){
                          echo '<div class="clear"></div>';
                        }
                      } 
                    }
                  ?>
                </div>
                <?php
              }
            ?>
          </div>
      </div>
      <div class="clear h40"></div>      
    </div>
</div>
</script>

<script type="text/javascript">
  document.title = "Образование в Калининграде | О рейтинге НОКО";
</script>
