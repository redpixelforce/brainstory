<?php
use yii\helpers\Html;
use  yii\bootstrap\ActiveForm;
use app\models\ChatWidget;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\helpers\Url
?>
<style>
    .row {
            padding-top: 5%;
                    padding-bottom: 5%;

    }
    .chat-text {
        text-align: center;
    }
    .col-md-6 {
        padding: 5%;
    }
</style>
<?php $f = ActiveForm::begin(['options' =>['enctype' => 'multipart/form-data']]); ?>

<div class="row">
    
    <div class="col-md-6">
    <?=$f->field($model, 'title')->label('Заголовок'); ?>

<?=$f->field($model, 'preview_text')->textArea(['rows' => 4, 'enableLabel' => true])->label('Текст превью'); ?> 
<?=$f->field($model, 'articleText')->textArea(['rows' => 6, 'enableLabel' => true,'id' => 'articleText'])->label('Полный текст'); ?> 
        <div class="controlPanel" style="margin-top: 5px;">
                <div class="form-group">
                  <button id="btnPasteBr"  type="button" class="btn btn-primary">Br</button>
                  <button id="btnPasteImg" type="button" class="btn btn-primary">Img</button>
                  <button id="btnPasteA" type="button" class="btn btn-primary">Ссылка</button>
                  <button id="btnPasteTable" type="button" class="btn btn-primary">Таблица</button>
                  <button id="btnPasteOL" type="button" class="btn btn-primary"><i class="fa fa-list-ol"></i></button>
                  <button id="btnPasteUL" type="button" class="btn btn-primary"><i class="fa fa-list-ul"></i></button>
                  <div class="clear"></div>
                  <button id="btnPasteP" type="button" class="btn btn-danger">Абзац</button>
                  <button id="btnPasteH1"type="button"  class="btn btn-danger">H1</button>
                  <button id="btnPasteH2" type="button" class="btn btn-danger">H2</button>
                  <button id="btnPasteH3" type="button" class="btn btn-danger">H3</button>
                  <button id="btnPasteStrong" type="button" class="btn btn-danger"><i class="fa fa-bold"></i></button>
                  <button id="btnPasteItalic" type="button" class="btn btn-danger"><i class="fa fa-italic"></i></button>
                  <button id="btnPasteUnderline"  type="button" class="btn btn-danger"><i class="fa fa-underline"></i></button>
                  <button id="btnPasteCenter" type="button" class="btn btn-danger"><i class="fa fa-align-center"></i></button>
                  <div class="clear"></div>
                  <button id="btnClear" type="button" class="btn btn-primary">Очистить поле</button>
                </div>
              </div>
        
    </div>
    <div class="col-md-6">
    

<!--

-->
        
<?=$f->field($model, 'tags')->label('Вставьте теги через пробел'); ?>
<?= $f->field($model, 'image')->widget(FileInput::classname(), [
     'name' => 'input-ru',
    'language' => 'ru',
    'options'=>[
    
        'multiple'=>false,
    'accept' => 'images/*',
    'id' => 'input1'
],
    'pluginEvents' => [
    "fileuploaded" => "function(event, data, previewId, index) { $('.file-input').after('<p class=\"label-load-image\"> Путь к изображению:'+data.response.response+'</p>'); }",
//    'fileloaded' => " function(event, file, previewId, index, reader){
//    if ($(this).closest('.file-input').find('.file-preview-frame').size() > 1)
//        $(this).closest('.file-input').find('.file-preview-frame:eq(0) .kv-file-remove').click(); }"
                            ],
      'pluginOptions' => [
        'allowedFileExtensions'=>['jpg','png'],
        
        'uploadUrl' => Url::to(['/site/file-upload']),
       'uploadExtraData' => [
            'type' => $type,
        
        ],
            'overwriteInitial'=>true,

            'maxFileCount' => 1

    ]
]); ?>
<?=Html::submitButton('Сохранить',['class' => 'btn btn-primary btn-sender', 'name' => 'add-button']); ?>
<?php ActiveForm::end(); ?>
    </div>

    
    </div>


