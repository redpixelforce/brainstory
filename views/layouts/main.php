<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\LoginFormWidget;
use app\models\RegistrationFormWidget;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    
<?php $this->beginBody() ?>
<?= (Yii::$app->user->isGuest ? LoginFormWidget::widget([]) : ''); ?>
<?= (Yii::$app->user->isGuest ? RegistrationFormWidget::widget([]) : ''); ?>


    
    
<div class="wrap">
    <style>
    
    .fa-sign-in:before {
	content: "\f090";
	font-family: FontAwesome;

	/* more styling for the icon, including color, font-size, positioning, etc. */
}
        
    </style>
    <?php
    NavBar::begin([
        'brandLabel' => '<img src="/images/logo.png" class="logo" style="margin-top: 14px">',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-fixed-top',
        ],
    
    ]);
 $menuItems = [
            ['label' => 'Главная', 'url' => ['/site/index']],
            ['label' => 'Статьи', 'url' => ['/site/articles']],
            ['label' => 'Игры', 'url' => ['/site/schools']],
            ['label' => 'Товары', 'url' => ['/site/aboutNOKO']],
        ];
            Yii::$app->user->isGuest ? (
//                 class="text-center" data-toggle="modal" data-target="#modalLogin"><i class="fa fa-sign-in" aria-hidden="true"
            $menuItems1[] = ['label' => 'Войти', 'url' => '#', 'options' => ['data-toggle' => 'modal', 'data-target' => '#login-modal'], 'linkOptions' => ['class' => 'fa fa-sign-in', 'aria-hidden' => 'true']]

            ) : (
               $menuItems1[] =    ['label' =>Yii::$app->user->identity->login,
            'items' => [
                 ['label' => 'Добавить статью или новость', 'url' => ['site/addarticle']],
                 ['label' => 'Добавить учебное заведение', 'url' => '#'],
                 ['label' => 'Добавить образовательный центр', 'url' => '#'],
    
                 ['label' => 'Выйти', 'url' => ['/site/logout'], 'linkOptions' =>  ['data-method' => 'POST']],

            ],
        ]
                );

    echo Nav::widget([
        'options' => ['class' => 'nav navbar-nav navbar-center'],
        'items' => $menuItems
        
        
    ]);
 echo Nav::widget([
        'options' => ['class' => 'nav navbar-nav navbar-right'],
        'items' => $menuItems1
        
        
    ]);
    NavBar::end();
    ?>

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    
    <?php
    $this->registerJs(                                   
    "$(function() {
       $('#modalButton a').click(function(e) {
       e.preventDefault();
       $('#modalLarge').modal('show')
         .find('.modal-content')
         .load($(this).attr('href'));
      });
     });", 
yii\web\View::POS_END, 'login-modal');
    
    ?>
      <?php
    $this->registerJs(                                   
    "$(function() {
       $('#modalButton a').click(function(e) {
       e.preventDefault();
       $('#modalLarge').modal('show')
         .find('.modal-content')
         .load($(this).attr('href'));
      });
     });", 
yii\web\View::POS_END, 'registration-modal');
    
    ?>
    
  
</div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>

 <div class="footer">
      <div class="container foot_container">
        <a class=" col-lg-4 col-md-4 col-sm-4 col-xs-12 foot-logo" href="#"  ><img src="/images/logo_footer.png" class="logo"  style="margin-top: 5px"></a>
        <h4 class="col-lg-4 col-md-4 col-sm-4 col-xs-12 foot-story text-center" >BrainStory<br>&copy; 2017</h4>
        <ul class="col-lg-4 col-md-4 col-sm-4 col-xs-12 foot-social list-inline text-right" >
          <li><a href="https://vk.com/edu39"><i class="fa fa-vk fa-2x" aria-hidden="true"></i></a></li>
          <li><a href="https://facebook.com/edu39.me"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a></li>
          <li><a href="https://instagram.com/edu39.me"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a></li>
        </ul>
      </div>
    </div>
<?php 
    yii\bootstrap\Modal::begin(['id'=>'modalLarge', 'size' => 'modal-lg']);
yii\bootstrap\Modal::end();

    ?>
<?php $this->endBody() ?>
</body>
</html>

<?php $this->endPage() ?>
