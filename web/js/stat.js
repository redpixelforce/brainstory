 $(document).ready(function(){
    $('#subj li').eq(1).addClass('selectedLink');
    sendRequest();

    $('#subj li').click(function(){
      $('#subj li, #noko li').removeClass('selectedLink');
      $('#year .btn').removeClass('disabled');
      $(this).addClass('selectedLink');
      sendRequest();
    });

    $('#year .btn').click(function(){
      if(!$(this).hasClass('disabled')){
        $('#year .btn').removeClass('btn-edu').addClass('btn-default');
        $(this).removeClass('btn-default').addClass('btn-edu');
        sendRequest(); 
      }
    });

    $('#noko').click(function(){
      $('#year .btn').addClass('disabled');
    });

    

    function sendRequest(number){
      var subj = $('#subj li.selectedLink').data('subj');
    var url = $('#year .btn.btn-edu').data('url');
      var year = $('#year .btn.btn-edu').data('year');
      console.log(year);
      var number = number || 0;
      $.ajax({
        url: url,
        type: 'POST',
          dataType: 'json',
          data: {subject: subj, year: year, number: number},
          error: function(req, text, error){
            console.log('Хьюстон, у нас проблемы! ' + text + ' | ' + error);
          },
          success: function(json){
            console.log(json);
            $('#ratingTable').html("");
            var html = "";
            var title = subj == 'noko' ? json[0]['SubjName'] : json[0]['SubjName'] + " " + year
            html += '<div class="text-center"><h4>' + title + '</h4></div>';
            html += '<div class="rating-rows"><ul class="school-rating">';
            for(var i = 0; i < json.length; i++){
              var rating = +json[i]['Rate'];
              var extraClass = '';
              if(json[i]['IdSubject'] != 14){
                rating = rating.toFixed(2); 
              }
              switch (i){
                case 0:
                  extraClass = 'first-place';
                  break;
                case 1:
                  extraClass = 'second-place';
                  break;
                case 2:
                  extraClass = 'third-place';
                  break;
                default:
                  extraClass = 'simple-place';
              }
              html += '<li class="li-border"><span class="' + extraClass + '">' + (i + 1) + '</span><span class="linkrow"><a href="/school/' + json[i]['UrlName'] + '"> ' + json[i]['ShortName'] + '</a><span class="value pull-right">' + rating + '</span></span></li>';
              //html += '<tr><td>' + (i + 1) + '</td><td><a href="/school/' + json[i]['UrlName'] + '">' + json[i]['ShortName'] + '</a></td><td>' + rating + '</td></tr>';
            }
            html += '</ul></div>';
            $('#ratingTable').append(html);
          }
      });
    }
  });