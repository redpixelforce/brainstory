<?php

return [
    'adminEmail' => 'redpixelforce@gmail.com',
    'pathUploads' => realpath(dirname(__FILE__)).'/../web//',
      'modules' => [
        'message' => [
            'class' => 'thyseus\message\Module',
                'userModelClass' => '\app\models\User', // your User model. Needs to be ActiveRecord.
        ],
    ],
];
